﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace BusTickets
{
    public partial class BuyingForm : Form
    {
        private readonly string reportFile = "ReportTemplate.docx";
        public BuyingForm()
        {
            InitializeComponent();
        }

        private void BuyingForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.ClientInfo' table. You can move, or remove it, as needed.
            this.clientInfoTableAdapter.Fill(this.bus_TicketsDataSet.ClientInfo);
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.City' table. You can move, or remove it, as needed.
            this.cityTableAdapter.Fill(this.bus_TicketsDataSet.City);
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.FlightInfo' table. You can move, or remove it, as needed.
            this.flightInfoTableAdapter.Fill(this.bus_TicketsDataSet.FlightInfo);
            FileStream stream = new FileStream("id.txt", FileMode.Open);
            StreamReader reader = new StreamReader(stream);
            string id_user = reader.ReadToEnd();
            stream.Close();
            clientInfoBindingSource.Filter = $"id_user = {Convert.ToInt32(id_user)}";
            this.ControlBox = false;
            TB_name.Text = TB_name.Text.Trim();
            TB_surname.Text = TB_surname.Text.Trim();
            TB_phone.Text = TB_phone.Text.Trim();
        }

        private void buy_butt_Click(object sender, EventArgs e)
        {
            string connectString = ConfigurationManager.ConnectionStrings["BusTickets.Properties.Settings.Bus_TicketsConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectString);
            string phonePattern = @"^[0][1-9]{2}\d{3}\d{2}\d{2}$";
            if (Regex.IsMatch(TB_phone.Text, phonePattern) && Regex.IsMatch(TB_name.Text, @"^[а-яА-Я]") && Regex.IsMatch(TB_surname.Text, @"^[а-яА-Я]"))
            {
                using (SqlConnection connection = new SqlConnection(connectString))
                {
                    con.Open();
                    string sql = "SELECT * FROM TicketInfo";
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);


                    System.Data.DataTable dt = ds.Tables[0];
                    DataRow newRow = dt.NewRow();
                    newRow["id_client"] = Convert.ToInt32(textBox1.Text);
                    newRow["id_flight"] = Convert.ToInt32(textBox2.Text);
                    newRow["seat_number"] = Convert.ToInt32(CB_seat.SelectedItem);
                    dt.Rows.Add(newRow);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
                    adapter.Update(dt);
                    ds.Clear();
                    MessageBox.Show("Білет оформлено");
                }

                var wordApp = new Word.Application();
                wordApp.Visible = false;
                var wordDoc = wordApp.Documents.Open(Environment.CurrentDirectory + @"\" + reportFile);
                Random rnd = new Random();


                ReplaceWord("{flightNumber}", TB_flightNumber.Text, wordDoc);
                ReplaceWord("{cityFrom}", TB_cityFrom.Text, wordDoc);
                ReplaceWord("{cityTo}", TB_cityTo.Text, wordDoc);
                ReplaceWord("{address}", label1.Text, wordDoc);
                ReplaceWord("{departDate}", TB_departDate.Text, wordDoc);
                ReplaceWord("{departTime}", TB_departTime.Text, wordDoc);
                ReplaceWord("{travelTime}", label2.Text, wordDoc);
                ReplaceWord("{arrivedTime}", TB_arrivedTime.Text, wordDoc);
                ReplaceWord("{distance}", TB_distance.Text, wordDoc);
                ReplaceWord("{name", TB_name.Text.Trim(), wordDoc);
                ReplaceWord("surname", TB_surname.Text.Trim(), wordDoc);
                ReplaceWord("phone}", TB_phone.Text.Trim(), wordDoc);
                ReplaceWord("{seat}", CB_seat.SelectedItem.ToString(), wordDoc);
                ReplaceWord("{price}", TB_price.Text, wordDoc);
                wordDoc.SaveAs2($@"{Environment.CurrentDirectory}" + @"\" + "Білет " + TB_surname.Text.Trim() + rnd.Next(10000, 1000000) + ".docx");
                wordApp.Visible = true;
            }
            else MessageBox.Show("Некорректні дані!");
        }

        private void ReplaceWord(string place, string text, Microsoft.Office.Interop.Word.Document wordDoc)
        {
            var range = wordDoc.Content;
            range.Find.ClearFormatting();
            range.Find.Execute(FindText: place, ReplaceWith: text);
        }

        private void back_butt_Click(object sender, EventArgs e)
        {
            MainForm mform = new MainForm();
            if (this.Text == "Адміністратор")
            {
                mform.Text = "Адміністратор";
                mform.edit_butt.Visible = true;
            }
            else mform.Text = "Користувач";
            this.Hide();
            mform.Show();            
        }
    }
}
