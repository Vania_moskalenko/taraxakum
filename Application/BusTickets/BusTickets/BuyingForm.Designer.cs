﻿namespace BusTickets
{
    partial class BuyingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuyingForm));
            this.TB_flightNumber = new System.Windows.Forms.TextBox();
            this.flightInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bus_TicketsDataSet = new BusTickets.Bus_TicketsDataSet();
            this.TB_departDate = new System.Windows.Forms.TextBox();
            this.TB_departTime = new System.Windows.Forms.TextBox();
            this.TB_arrivedTime = new System.Windows.Forms.TextBox();
            this.TB_distance = new System.Windows.Forms.TextBox();
            this.TB_name = new System.Windows.Forms.TextBox();
            this.clientInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TB_surname = new System.Windows.Forms.TextBox();
            this.TB_phone = new System.Windows.Forms.TextBox();
            this.TB_price = new System.Windows.Forms.TextBox();
            this.cityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.flightInfoTableAdapter = new BusTickets.Bus_TicketsDataSetTableAdapters.FlightInfoTableAdapter();
            this.cityBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cityTableAdapter = new BusTickets.Bus_TicketsDataSetTableAdapters.CityTableAdapter();
            this.clientInfoTableAdapter = new BusTickets.Bus_TicketsDataSetTableAdapters.ClientInfoTableAdapter();
            this.TB_cityFrom = new System.Windows.Forms.TextBox();
            this.TB_cityTo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buy_butt = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.CB_seat = new System.Windows.Forms.ComboBox();
            this.back_butt = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.flightInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bus_TicketsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_flightNumber
            // 
            this.TB_flightNumber.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.flightInfoBindingSource, "flight_number", true));
            this.TB_flightNumber.Enabled = false;
            this.TB_flightNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_flightNumber.Location = new System.Drawing.Point(62, 12);
            this.TB_flightNumber.Name = "TB_flightNumber";
            this.TB_flightNumber.Size = new System.Drawing.Size(100, 23);
            this.TB_flightNumber.TabIndex = 0;
            // 
            // flightInfoBindingSource
            // 
            this.flightInfoBindingSource.DataMember = "FlightInfo";
            this.flightInfoBindingSource.DataSource = this.bus_TicketsDataSet;
            // 
            // bus_TicketsDataSet
            // 
            this.bus_TicketsDataSet.DataSetName = "Bus_TicketsDataSet";
            this.bus_TicketsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TB_departDate
            // 
            this.TB_departDate.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.flightInfoBindingSource, "depart_date", true));
            this.TB_departDate.Enabled = false;
            this.TB_departDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_departDate.Location = new System.Drawing.Point(62, 139);
            this.TB_departDate.Name = "TB_departDate";
            this.TB_departDate.Size = new System.Drawing.Size(100, 23);
            this.TB_departDate.TabIndex = 3;
            // 
            // TB_departTime
            // 
            this.TB_departTime.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.flightInfoBindingSource, "depart_time", true));
            this.TB_departTime.Enabled = false;
            this.TB_departTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_departTime.Location = new System.Drawing.Point(62, 178);
            this.TB_departTime.Name = "TB_departTime";
            this.TB_departTime.Size = new System.Drawing.Size(100, 23);
            this.TB_departTime.TabIndex = 4;
            // 
            // TB_arrivedTime
            // 
            this.TB_arrivedTime.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.flightInfoBindingSource, "arrived_time", true));
            this.TB_arrivedTime.Enabled = false;
            this.TB_arrivedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_arrivedTime.Location = new System.Drawing.Point(62, 220);
            this.TB_arrivedTime.Name = "TB_arrivedTime";
            this.TB_arrivedTime.Size = new System.Drawing.Size(100, 23);
            this.TB_arrivedTime.TabIndex = 5;
            // 
            // TB_distance
            // 
            this.TB_distance.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.flightInfoBindingSource, "distance", true));
            this.TB_distance.Enabled = false;
            this.TB_distance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_distance.Location = new System.Drawing.Point(258, 10);
            this.TB_distance.Name = "TB_distance";
            this.TB_distance.Size = new System.Drawing.Size(100, 23);
            this.TB_distance.TabIndex = 6;
            // 
            // TB_name
            // 
            this.TB_name.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientInfoBindingSource, "name", true));
            this.TB_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_name.Location = new System.Drawing.Point(258, 53);
            this.TB_name.Name = "TB_name";
            this.TB_name.Size = new System.Drawing.Size(100, 23);
            this.TB_name.TabIndex = 7;
            // 
            // clientInfoBindingSource
            // 
            this.clientInfoBindingSource.DataMember = "ClientInfo";
            this.clientInfoBindingSource.DataSource = this.bus_TicketsDataSet;
            // 
            // TB_surname
            // 
            this.TB_surname.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientInfoBindingSource, "surname", true));
            this.TB_surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_surname.Location = new System.Drawing.Point(258, 92);
            this.TB_surname.Name = "TB_surname";
            this.TB_surname.Size = new System.Drawing.Size(100, 23);
            this.TB_surname.TabIndex = 8;
            // 
            // TB_phone
            // 
            this.TB_phone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientInfoBindingSource, "phone", true));
            this.TB_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_phone.Location = new System.Drawing.Point(258, 134);
            this.TB_phone.Name = "TB_phone";
            this.TB_phone.Size = new System.Drawing.Size(100, 23);
            this.TB_phone.TabIndex = 9;
            // 
            // TB_price
            // 
            this.TB_price.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.flightInfoBindingSource, "ticket_price", true));
            this.TB_price.Enabled = false;
            this.TB_price.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_price.Location = new System.Drawing.Point(258, 217);
            this.TB_price.Name = "TB_price";
            this.TB_price.Size = new System.Drawing.Size(100, 23);
            this.TB_price.TabIndex = 11;
            // 
            // cityBindingSource
            // 
            this.cityBindingSource.DataMember = "City";
            this.cityBindingSource.DataSource = this.bus_TicketsDataSet;
            // 
            // flightInfoTableAdapter
            // 
            this.flightInfoTableAdapter.ClearBeforeFill = true;
            // 
            // cityBindingSource1
            // 
            this.cityBindingSource1.DataMember = "City";
            this.cityBindingSource1.DataSource = this.bus_TicketsDataSet;
            // 
            // cityTableAdapter
            // 
            this.cityTableAdapter.ClearBeforeFill = true;
            // 
            // clientInfoTableAdapter
            // 
            this.clientInfoTableAdapter.ClearBeforeFill = true;
            // 
            // TB_cityFrom
            // 
            this.TB_cityFrom.Enabled = false;
            this.TB_cityFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_cityFrom.Location = new System.Drawing.Point(62, 55);
            this.TB_cityFrom.Name = "TB_cityFrom";
            this.TB_cityFrom.Size = new System.Drawing.Size(100, 23);
            this.TB_cityFrom.TabIndex = 15;
            // 
            // TB_cityTo
            // 
            this.TB_cityTo.Enabled = false;
            this.TB_cityTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_cityTo.Location = new System.Drawing.Point(62, 94);
            this.TB_cityTo.Name = "TB_cityTo";
            this.TB_cityTo.Size = new System.Drawing.Size(100, 23);
            this.TB_cityTo.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(53, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 19);
            this.label1.TabIndex = 18;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(50, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 16);
            this.label2.TabIndex = 19;
            this.label2.Text = "label2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buy_butt
            // 
            this.buy_butt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buy_butt.BackgroundImage")));
            this.buy_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buy_butt.Location = new System.Drawing.Point(189, 255);
            this.buy_butt.Name = "buy_butt";
            this.buy_butt.Size = new System.Drawing.Size(92, 39);
            this.buy_butt.TabIndex = 20;
            this.buy_butt.UseVisualStyleBackColor = true;
            this.buy_butt.Click += new System.EventHandler(this.buy_butt_Click);
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientInfoBindingSource, "id_client", true));
            this.textBox1.Location = new System.Drawing.Point(312, 219);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1, 20);
            this.textBox1.TabIndex = 21;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.flightInfoBindingSource, "id_flight", true));
            this.textBox2.Location = new System.Drawing.Point(280, 219);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(1, 20);
            this.textBox2.TabIndex = 22;
            // 
            // CB_seat
            // 
            this.CB_seat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CB_seat.FormattingEnabled = true;
            this.CB_seat.Location = new System.Drawing.Point(258, 176);
            this.CB_seat.Name = "CB_seat";
            this.CB_seat.Size = new System.Drawing.Size(100, 24);
            this.CB_seat.TabIndex = 23;
            // 
            // back_butt
            // 
            this.back_butt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("back_butt.BackgroundImage")));
            this.back_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.back_butt.Location = new System.Drawing.Point(77, 255);
            this.back_butt.Name = "back_butt";
            this.back_butt.Size = new System.Drawing.Size(92, 38);
            this.back_butt.TabIndex = 24;
            this.back_butt.UseVisualStyleBackColor = true;
            this.back_butt.Click += new System.EventHandler(this.back_butt_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Рейс";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Звідки";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Куди";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Дата";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(10, 171);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 36);
            this.label7.TabIndex = 26;
            this.label7.Text = "Відправлення";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 227);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Прибуття";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(205, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Відстань";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(215, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Ім\'я";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(200, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Прізвище";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(199, 139);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Телефон";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(207, 178);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Місце";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(210, 222);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Ціна";
            // 
            // BuyingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 311);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.back_butt);
            this.Controls.Add(this.CB_seat);
            this.Controls.Add(this.buy_butt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB_cityTo);
            this.Controls.Add(this.TB_cityFrom);
            this.Controls.Add(this.TB_price);
            this.Controls.Add(this.TB_phone);
            this.Controls.Add(this.TB_surname);
            this.Controls.Add(this.TB_name);
            this.Controls.Add(this.TB_distance);
            this.Controls.Add(this.TB_arrivedTime);
            this.Controls.Add(this.TB_departTime);
            this.Controls.Add(this.TB_departDate);
            this.Controls.Add(this.TB_flightNumber);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.MaximumSize = new System.Drawing.Size(377, 350);
            this.MinimumSize = new System.Drawing.Size(377, 350);
            this.Name = "BuyingForm";
            this.Text = "Замовлення";
            this.Load += new System.EventHandler(this.BuyingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flightInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bus_TicketsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox TB_flightNumber;
        public System.Windows.Forms.TextBox TB_departDate;
        public System.Windows.Forms.TextBox TB_departTime;
        public System.Windows.Forms.TextBox TB_arrivedTime;
        public System.Windows.Forms.TextBox TB_distance;
        public System.Windows.Forms.TextBox TB_name;
        public System.Windows.Forms.TextBox TB_surname;
        public System.Windows.Forms.TextBox TB_phone;
        public System.Windows.Forms.TextBox TB_price;
        public Bus_TicketsDataSet bus_TicketsDataSet;
        public System.Windows.Forms.BindingSource flightInfoBindingSource;
        public Bus_TicketsDataSetTableAdapters.FlightInfoTableAdapter flightInfoTableAdapter;
        private System.Windows.Forms.BindingSource cityBindingSource;
        private Bus_TicketsDataSetTableAdapters.CityTableAdapter cityTableAdapter;
        private System.Windows.Forms.BindingSource clientInfoBindingSource;
        private Bus_TicketsDataSetTableAdapters.ClientInfoTableAdapter clientInfoTableAdapter;
        private System.Windows.Forms.BindingSource cityBindingSource1;
        public System.Windows.Forms.TextBox TB_cityFrom;
        public System.Windows.Forms.TextBox TB_cityTo;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button buy_butt;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.ComboBox CB_seat;
        private System.Windows.Forms.Button back_butt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
    }
}