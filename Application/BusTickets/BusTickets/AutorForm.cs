﻿using BusTickets.Bus_TicketsDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusTickets
{
    public partial class AutorForm : Form
    {
        public string id_user = "string";
        public AutorForm()
        {
            InitializeComponent();
            
        }    

        public void log_button_Click(object sender, EventArgs e)
        {
            var k = 0;          
            for (int i = 0; i < this.bus_TicketsDataSet.Account.Count; i++)
            {
                if (this.bus_TicketsDataSet.Account.Rows[i]["login"].ToString().Trim() == TB_login.Text && this.bus_TicketsDataSet.Account.Rows[i]["password"].ToString().Trim() == TB_pass.Text)
                {
                    if (this.bus_TicketsDataSet.Account.Rows[i]["role"].ToString().Trim() == "admin")
                    {
                        k++;
                        this.Hide();
                        File.WriteAllText("id.txt", $"{this.bus_TicketsDataSet.Account.Rows[i]["id_user"].ToString().Trim()}");
                        MainForm mainform = new MainForm();
                        mainform.edit_butt.Visible = true;
                        mainform.Text = "Адміністратор";
                        HomeForm homeform= new HomeForm();                                                                    
                        mainform.Show();
                        
                    }
                    else
                    {
                        k++;
                        HomeForm homeform = new HomeForm();

                        File.WriteAllText("id.txt", $"{this.bus_TicketsDataSet.Account.Rows[i]["id_user"].ToString().Trim()}");

                        this.Hide();
                        MainForm mainform = new MainForm();                                                                                    
                        mainform.Text = "Користувач"; 
                        mainform.Show();
                    }
                }                                   
            }
            if (k==0)
                MessageBox.Show("Некорректні дані");
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                this.accountTableAdapter1.Fill(this.bus_TicketsDataSet.Account);
            }
            catch (SqlException)
            {
                MessageBox.Show("Не вдається під'єднатися до БД");
                Application.Exit();
            }
        }

        private void reg_button_Click(object sender, EventArgs e)
        {
            RegForm rform = new RegForm();
            this.Hide();
            rform.Show();
        }
    }
}
