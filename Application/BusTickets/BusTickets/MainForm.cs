﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusTickets
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.City' table. You can move, or remove it, as needed.
            this.cityTableAdapter.Fill(this.bus_TicketsDataSet.City);
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.FlightInfo' table. You can move, or remove it, as needed.
            this.flightInfoTableAdapter.Fill(this.bus_TicketsDataSet.FlightInfo);

            int[] numbers = new int[50];
            var cont = 0;
            string connectString = ConfigurationManager.ConnectionStrings["BusTickets.Properties.Settings.Bus_TicketsConnectionString"].ConnectionString;            
            SqlConnection con = new SqlConnection(connectString);
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter("select distinct flight_number from FlightInfo", con);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    numbers[cont] = row.Field<int>(0);
                    cont++;
                }
               
                for (int i = 0; i < numbers.Length; i++)
                {
                    if (numbers[i]!=0)
                    CB_flightNumber.Items.Add(numbers[i]);
                }
                CB_flightNumber.SelectedIndex = 0;
            }
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void search_button_Click(object sender, EventArgs e)
        {
            try
            {
                
                //one param
                if (check_flight.Checked == false && check_from.Checked == false && check_to.Checked == false && check_date.Checked == false)
                {
                    MessageBox.Show("Оберіть параметри пошуку!");
                }
                 if (check_flight.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"flight_number = '{CB_flightNumber.Text}'";
                }
                 if (check_from.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"id_city_from = '{CB_cityFrom.SelectedValue}'";
                }
                 if (check_to.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"id_city_to = '{CB_cityTo.SelectedValue}'";
                }
                 if (check_date.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"depart_date = '{DTP_depart.Value}'";
                }
                //two param - flight
                 if (check_flight.Checked == true && check_from.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"flight_number = '{CB_flightNumber.Text}' and id_city_from = '{CB_cityFrom.SelectedValue}'";
                }
                 if (check_flight.Checked == true && check_to.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"flight_number = '{CB_flightNumber.Text}' and id_city_to = '{CB_cityTo.SelectedValue}'";
                }
                 if (check_flight.Checked == true && check_date.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"flight_number = '{CB_flightNumber.Text}' and depart_date = '{DTP_depart.Value}'";
                }
                //two param - from
                 if (check_from.Checked == true && check_to.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"id_city_from = '{CB_cityFrom.SelectedValue}' and id_city_to = '{CB_cityTo.SelectedValue}'";
                }
                 if (check_from.Checked == true && check_date.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"id_city_from = '{CB_cityFrom.SelectedValue}' and depart_date = '{DTP_depart.Value}'";
                }
                //two param - to
                 if (check_to.Checked == true && check_date.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"id_city_to = '{CB_cityTo.SelectedValue}' and depart_date = '{DTP_depart.Value}'";
                }
                //three param - flight, from, to
                 if (check_flight.Checked == true && check_from.Checked == true && check_to.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"flight_number = '{CB_flightNumber.Text}' and id_city_from = '{CB_cityFrom.SelectedValue}' and id_city_to = '{CB_cityTo.SelectedValue}'";
                }
                //three param - flight, from, date
                 if (check_flight.Checked == true && check_from.Checked == true && check_date.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"flight_number = '{CB_flightNumber.Text}' and id_city_from = '{CB_cityFrom.SelectedValue}' and depart_date = '{DTP_depart.Value}'";
                }
                //three param - flight, to, date
                 if (check_flight.Checked == true && check_to.Checked == true && check_date.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"flight_number = '{CB_flightNumber.Text}' and id_city_to = '{CB_cityTo.SelectedValue}' and depart_date = '{DTP_depart.Value}'";
                }
                //three param - from, to, date
                 if (check_from.Checked == true && check_to.Checked == true && check_date.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"id_city_from = '{CB_cityFrom.SelectedValue}' and id_city_to = '{CB_cityTo.SelectedValue}' and depart_date = '{DTP_depart.Value}'";
                }
                //four param
                 if (check_flight.Checked == true && check_from.Checked == true && check_to.Checked == true && check_date.Checked == true)
                {
                    flightInfoBindingSource.Filter = $"flight_number = '{CB_flightNumber.Text}' and id_city_from = '{CB_cityFrom.SelectedValue}' and id_city_to = '{CB_cityTo.SelectedValue}' and depart_date = '{DTP_depart.Value}'";
                }
                
            }
            catch (System.Data.EvaluateException)
            {
                MessageBox.Show("Помилка пошуку!");
            }
        }

        private void clear_button_Click(object sender, EventArgs e)
        {
            flightInfoBindingSource.Filter = "";
        }

        private void home_butt_Click(object sender, EventArgs e)
        {
            HomeForm homeform = new HomeForm();
            this.Hide();
            if (this.Text == "Адміністратор")
            {
                homeform.Text = "Адміністратор";
                
            }
            else {
                homeform.Text = "Користувач";
            }
            homeform.Show();
        }

        private void buy_butt_Click(object sender, EventArgs e)
        {
            
            BuyingForm bform = new BuyingForm();
            if (this.Text == "Адміністратор")
            {
                bform.Text = "Адміністратор";
            }
            this.Hide();
            bform.Show();
            bform.flightInfoBindingSource.Position = this.flightInfoBindingSource.Position;
            bform.TB_cityFrom.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString();
            bform.TB_cityTo.Text = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].FormattedValue.ToString();

            int seatamount = 0;
            int[] numbers = new int[50];
            int cont = 0;
            string connectString = ConfigurationManager.ConnectionStrings["BusTickets.Properties.Settings.Bus_TicketsConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectString);
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                con.Open();
                SqlCommand command = new SqlCommand($"select address from City where id_city = {Convert.ToUInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].Value)}", con);
                bform.label1.Text = command.ExecuteScalar().ToString().Trim();

                command = new SqlCommand($"select seats_amount from FlightInfo where id_flight = {Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value)}", con);
                seatamount = Convert.ToInt32(command.ExecuteScalar());

                
                SqlDataAdapter adapter = new SqlDataAdapter("select * from TicketInfo", connection);
                command = new SqlCommand($"select seat_number from TicketInfo where id_flight = { Convert.ToInt32(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value)}", con);
                adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    numbers[cont] = row.Field<int>(0);
                    cont++;                               
                }


                for (int j = 1; j < seatamount + 1; j++)
                {
                    bform.CB_seat.Items.Add(j);
                }

                for (int i = 0; i < numbers.Length; i++)
                {
                    bform.CB_seat.Items.Remove(numbers[i]);
                }
            }
            bform.label2.Text ="В дорозі: " + (Convert.ToDateTime(bform.TB_arrivedTime.Text) - Convert.ToDateTime(bform.TB_departTime.Text)).ToString();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void edit_butt_Click(object sender, EventArgs e)
        {
            FlightEdit fedit = new FlightEdit();
            this.Hide();
            fedit.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (this.Text == "Адміністратор")
            {
                DialogResult answer = MessageBox.Show("Ви дійсно хочете видалити рейс?", "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer == DialogResult.Yes)
                {
                    flightInfoBindingSource.Filter = $"id_flight = '{dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value}'";
                    flightInfoBindingSource.RemoveCurrent();
                    flightInfoBindingSource.EndEdit();
                    flightInfoTableAdapter.Update(bus_TicketsDataSet);
                    dataGridView1.Update();
                    this.Update();
                    flightInfoBindingSource.Filter = "";
                }
            }
            else
                MessageBox.Show("Недостатньо прав!");
        }
    }
}
