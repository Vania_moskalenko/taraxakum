﻿namespace BusTickets
{
    partial class AutorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutorForm));
            this.TB_login = new System.Windows.Forms.TextBox();
            this.TB_pass = new System.Windows.Forms.TextBox();
            this.login_label = new System.Windows.Forms.Label();
            this.pass_label = new System.Windows.Forms.Label();
            this.reg_button = new System.Windows.Forms.Button();
            this.log_button = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bus_TicketsDataSet = new BusTickets.Bus_TicketsDataSet();
            this.accountTableAdapter1 = new BusTickets.Bus_TicketsDataSetTableAdapters.AccountTableAdapter();
            this.clientInfoTableAdapter1 = new BusTickets.Bus_TicketsDataSetTableAdapters.ClientInfoTableAdapter();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bus_TicketsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_login
            // 
            this.TB_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_login.Location = new System.Drawing.Point(159, 39);
            this.TB_login.Name = "TB_login";
            this.TB_login.Size = new System.Drawing.Size(108, 29);
            this.TB_login.TabIndex = 0;
            // 
            // TB_pass
            // 
            this.TB_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_pass.Location = new System.Drawing.Point(159, 95);
            this.TB_pass.Name = "TB_pass";
            this.TB_pass.PasswordChar = '*';
            this.TB_pass.Size = new System.Drawing.Size(108, 29);
            this.TB_pass.TabIndex = 1;
            // 
            // login_label
            // 
            this.login_label.AutoSize = true;
            this.login_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.login_label.Location = new System.Drawing.Point(71, 39);
            this.login_label.Name = "login_label";
            this.login_label.Size = new System.Drawing.Size(62, 24);
            this.login_label.TabIndex = 2;
            this.login_label.Text = "Логін";
            // 
            // pass_label
            // 
            this.pass_label.AutoSize = true;
            this.pass_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pass_label.Location = new System.Drawing.Point(71, 98);
            this.pass_label.Name = "pass_label";
            this.pass_label.Size = new System.Drawing.Size(82, 24);
            this.pass_label.TabIndex = 3;
            this.pass_label.Text = "Пароль";
            // 
            // reg_button
            // 
            this.reg_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.reg_button.Location = new System.Drawing.Point(31, 163);
            this.reg_button.Name = "reg_button";
            this.reg_button.Size = new System.Drawing.Size(108, 29);
            this.reg_button.TabIndex = 4;
            this.reg_button.Text = "Реєстрація";
            this.reg_button.UseVisualStyleBackColor = true;
            this.reg_button.Click += new System.EventHandler(this.reg_button_Click);
            // 
            // log_button
            // 
            this.log_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.log_button.Location = new System.Drawing.Point(159, 163);
            this.log_button.Name = "log_button";
            this.log_button.Size = new System.Drawing.Size(108, 29);
            this.log_button.TabIndex = 5;
            this.log_button.Text = "Увійти";
            this.log_button.UseVisualStyleBackColor = true;
            this.log_button.Click += new System.EventHandler(this.log_button_Click);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = this.bus_TicketsDataSet;
            this.bindingSource1.Position = 0;
            // 
            // bus_TicketsDataSet
            // 
            this.bus_TicketsDataSet.DataSetName = "Bus_TicketsDataSet";
            this.bus_TicketsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // accountTableAdapter1
            // 
            this.accountTableAdapter1.ClearBeforeFill = true;
            // 
            // clientInfoTableAdapter1
            // 
            this.clientInfoTableAdapter1.ClearBeforeFill = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(15, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(53, 41);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.InitialImage = null;
            this.pictureBox2.Location = new System.Drawing.Point(15, 83);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(53, 41);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // AutorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 211);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.log_button);
            this.Controls.Add(this.reg_button);
            this.Controls.Add(this.pass_label);
            this.Controls.Add(this.login_label);
            this.Controls.Add(this.TB_pass);
            this.Controls.Add(this.TB_login);
            this.MaximumSize = new System.Drawing.Size(300, 250);
            this.MinimumSize = new System.Drawing.Size(300, 250);
            this.Name = "AutorForm";
            this.Text = "Авторизація";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bus_TicketsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox TB_login;
        public System.Windows.Forms.TextBox TB_pass;
        public System.Windows.Forms.Label login_label;
        public System.Windows.Forms.Label pass_label;
        public System.Windows.Forms.Button reg_button;
        public System.Windows.Forms.Button log_button;
        public System.Windows.Forms.BindingSource bindingSource1;
        public Bus_TicketsDataSet bus_TicketsDataSet;
        public Bus_TicketsDataSetTableAdapters.AccountTableAdapter accountTableAdapter1;
        public Bus_TicketsDataSetTableAdapters.ClientInfoTableAdapter clientInfoTableAdapter1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

