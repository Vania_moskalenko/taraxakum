﻿namespace BusTickets
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cancel_butt = new System.Windows.Forms.Button();
            this.back_butt = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TB_phone = new System.Windows.Forms.MaskedTextBox();
            this.clientInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bus_TicketsDataSet = new BusTickets.Bus_TicketsDataSet();
            this.save_butt = new System.Windows.Forms.Button();
            this.edit_butt = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TB_surname = new System.Windows.Forms.TextBox();
            this.TB_name = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.del_butt = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.clientInfoTableAdapter = new BusTickets.Bus_TicketsDataSetTableAdapters.ClientInfoTableAdapter();
            this.ticketInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ticketInfoTableAdapter = new BusTickets.Bus_TicketsDataSetTableAdapters.TicketInfoTableAdapter();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bus_TicketsDataSet)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketInfoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-5, 2);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(486, 313);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cancel_butt);
            this.tabPage1.Controls.Add(this.back_butt);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.TB_phone);
            this.tabPage1.Controls.Add(this.save_butt);
            this.tabPage1.Controls.Add(this.edit_butt);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.TB_surname);
            this.tabPage1.Controls.Add(this.TB_name);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(478, 287);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Профіль";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cancel_butt
            // 
            this.cancel_butt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cancel_butt.BackgroundImage")));
            this.cancel_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cancel_butt.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancel_butt.Location = new System.Drawing.Point(217, 222);
            this.cancel_butt.Name = "cancel_butt";
            this.cancel_butt.Size = new System.Drawing.Size(116, 29);
            this.cancel_butt.TabIndex = 11;
            this.cancel_butt.UseVisualStyleBackColor = true;
            this.cancel_butt.Visible = false;
            this.cancel_butt.Click += new System.EventHandler(this.button1_Click);
            // 
            // back_butt
            // 
            this.back_butt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("back_butt.BackgroundImage")));
            this.back_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.back_butt.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back_butt.Location = new System.Drawing.Point(1, 263);
            this.back_butt.Name = "back_butt";
            this.back_butt.Size = new System.Drawing.Size(37, 23);
            this.back_butt.TabIndex = 9;
            this.back_butt.UseVisualStyleBackColor = true;
            this.back_butt.Click += new System.EventHandler(this.back_butt_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(189, 223);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // TB_phone
            // 
            this.TB_phone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientInfoBindingSource, "phone", true));
            this.TB_phone.Enabled = false;
            this.TB_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_phone.Location = new System.Drawing.Point(311, 134);
            this.TB_phone.Name = "TB_phone";
            this.TB_phone.Size = new System.Drawing.Size(147, 24);
            this.TB_phone.TabIndex = 8;
            this.TB_phone.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.TB_phone_MaskInputRejected);
            // 
            // clientInfoBindingSource
            // 
            this.clientInfoBindingSource.DataMember = "ClientInfo";
            this.clientInfoBindingSource.DataSource = this.bus_TicketsDataSet;
            // 
            // bus_TicketsDataSet
            // 
            this.bus_TicketsDataSet.DataSetName = "Bus_TicketsDataSet";
            this.bus_TicketsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // save_butt
            // 
            this.save_butt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("save_butt.BackgroundImage")));
            this.save_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.save_butt.Cursor = System.Windows.Forms.Cursors.Hand;
            this.save_butt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.save_butt.Location = new System.Drawing.Point(339, 221);
            this.save_butt.Name = "save_butt";
            this.save_butt.Size = new System.Drawing.Size(119, 30);
            this.save_butt.TabIndex = 7;
            this.save_butt.UseVisualStyleBackColor = true;
            this.save_butt.Visible = false;
            this.save_butt.Click += new System.EventHandler(this.save_butt_Click);
            // 
            // edit_butt
            // 
            this.edit_butt.Cursor = System.Windows.Forms.Cursors.Hand;
            this.edit_butt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edit_butt.Location = new System.Drawing.Point(217, 181);
            this.edit_butt.Name = "edit_butt";
            this.edit_butt.Size = new System.Drawing.Size(242, 30);
            this.edit_butt.TabIndex = 6;
            this.edit_butt.Text = "Редагувати";
            this.edit_butt.UseVisualStyleBackColor = true;
            this.edit_butt.Click += new System.EventHandler(this.edit_butt_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(214, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 45);
            this.label3.TabIndex = 5;
            this.label3.Text = "Номер телефону";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(214, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Прізвище";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(239, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ім\'я";
            // 
            // TB_surname
            // 
            this.TB_surname.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientInfoBindingSource, "surname", true));
            this.TB_surname.Enabled = false;
            this.TB_surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_surname.Location = new System.Drawing.Point(311, 82);
            this.TB_surname.Name = "TB_surname";
            this.TB_surname.Size = new System.Drawing.Size(148, 24);
            this.TB_surname.TabIndex = 1;
            // 
            // TB_name
            // 
            this.TB_name.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientInfoBindingSource, "name", true));
            this.TB_name.Enabled = false;
            this.TB_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TB_name.Location = new System.Drawing.Point(311, 30);
            this.TB_name.Name = "TB_name";
            this.TB_name.Size = new System.Drawing.Size(148, 24);
            this.TB_name.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.del_butt);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(478, 287);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Історія";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // del_butt
            // 
            this.del_butt.Cursor = System.Windows.Forms.Cursors.Hand;
            this.del_butt.Enabled = false;
            this.del_butt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.del_butt.Location = new System.Drawing.Point(160, 238);
            this.del_butt.Name = "del_butt";
            this.del_butt.Size = new System.Drawing.Size(129, 42);
            this.del_butt.TabIndex = 1;
            this.del_butt.Text = "Повернути квиток";
            this.del_butt.UseVisualStyleBackColor = true;
            this.del_butt.Click += new System.EventHandler(this.del_butt_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(9, 6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(457, 226);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // clientInfoTableAdapter
            // 
            this.clientInfoTableAdapter.ClearBeforeFill = true;
            // 
            // ticketInfoBindingSource
            // 
            this.ticketInfoBindingSource.DataMember = "TicketInfo";
            this.ticketInfoBindingSource.DataSource = this.bus_TicketsDataSet;
            // 
            // ticketInfoTableAdapter
            // 
            this.ticketInfoTableAdapter.ClearBeforeFill = true;
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 323);
            this.Controls.Add(this.tabControl1);
            this.MaximumSize = new System.Drawing.Size(485, 350);
            this.MinimumSize = new System.Drawing.Size(485, 350);
            this.Name = "HomeForm";
            this.Text = "Профіль";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.HomeForm_FormClosed);
            this.Load += new System.EventHandler(this.HomeForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bus_TicketsDataSet)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketInfoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.Button edit_butt;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox TB_surname;
        public System.Windows.Forms.TextBox TB_name;
        public System.Windows.Forms.TabPage tabPage2;
        public Bus_TicketsDataSet bus_TicketsDataSet;
        public System.Windows.Forms.BindingSource clientInfoBindingSource;
        public Bus_TicketsDataSetTableAdapters.ClientInfoTableAdapter clientInfoTableAdapter;
        private System.Windows.Forms.Button save_butt;
        private System.Windows.Forms.MaskedTextBox TB_phone;
        private System.Windows.Forms.BindingSource ticketInfoBindingSource;
        private Bus_TicketsDataSetTableAdapters.TicketInfoTableAdapter ticketInfoTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button del_butt;
        private System.Windows.Forms.Button back_butt;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button cancel_butt;
    }
}