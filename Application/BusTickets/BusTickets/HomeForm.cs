﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusTickets
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }


        public void HomeForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.FlightInfo' table. You can move, or remove it, as needed.
            //this.flightInfoTableAdapter.Fill(this.bus_TicketsDataSet.FlightInfo);
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.TicketInfo' table. You can move, or remove it, as needed.
            this.ticketInfoTableAdapter.Fill(this.bus_TicketsDataSet.TicketInfo);

            // TODO: This line of code loads data into the 'bus_TicketsDataSet.ClientInfo' table. You can move, or remove it, as needed.
            this.clientInfoTableAdapter.Fill(this.bus_TicketsDataSet.ClientInfo);
            AutorForm aform = new AutorForm();

            FileStream stream = new FileStream("id.txt", FileMode.Open);
            StreamReader reader = new StreamReader(stream);
            string id_user = reader.ReadToEnd();
            stream.Close();
            clientInfoBindingSource.Filter = $"id_user = {Convert.ToInt32(id_user)}";
            this.ControlBox = false;
            TB_name.Text = TB_name.Text.Trim();
            TB_surname.Text = TB_surname.Text.Trim();
            TB_phone.Text = TB_phone.Text.Trim();
            
        }
        public void edit_butt_Click(object sender, EventArgs e)
        {
            TB_name.Enabled = true;
            TB_phone.Enabled = true;
            TB_surname.Enabled = true;
            save_butt.Visible = true;
            cancel_butt.Visible = true;

        }

        private void save_butt_Click(object sender, EventArgs e)
        {
            var tmpPhone = TB_phone.Text.Replace(" ", "");
            var tmpName = TB_name.Text.Replace(" ", "");
            var tmpSurname = TB_surname.Text.Replace(" ", "");
            string phonePattern = @"^[0][1-9]{2}\d{3}\d{2}\d{2}";

            if (Regex.IsMatch(tmpPhone, phonePattern) && Regex.IsMatch(tmpName, @"^[а-яА-Я]") && Regex.IsMatch(tmpSurname, @"^[а-яА-Я]"))
            {
                clientInfoBindingSource.EndEdit();
                clientInfoTableAdapter.Update(bus_TicketsDataSet);
                TB_name.Enabled = false;
                TB_phone.Enabled = false;
                TB_surname.Enabled = false;
                save_butt.Visible = false;
                cancel_butt.Visible = false;
            }
            else
            {            
                MessageBox.Show("Некоректні дані!");
                tmpPhone = TB_phone.Text.Replace(" ", "");
                tmpName = TB_name.Text.Replace(" ", "");
                tmpSurname = TB_surname.Text.Replace(" ", "");
            }
        }

        public void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Text == "Історія")
            {               
                FileStream stream = new FileStream("id.txt", FileMode.Open);
                StreamReader reader = new StreamReader(stream);
                string id_user = reader.ReadToEnd();
                stream.Close();

                var id_client = 0;
                for (int i = 0; i < this.bus_TicketsDataSet.ClientInfo.Count; i++)
                {
                    if (this.bus_TicketsDataSet.ClientInfo.Rows[i]["id_user"].ToString().Trim() == id_user)
                    {
                        id_client = Convert.ToInt32(this.bus_TicketsDataSet.ClientInfo.Rows[i]["id_client"]);
                    }
                }               
                string connectString = ConfigurationManager.ConnectionStrings["BusTickets.Properties.Settings.Bus_TicketsConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(connectString);
                using (SqlConnection connection = new SqlConnection(connectString))
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter($"select TicketInfo.id_flight, flight_number as Рейс, city_name as Місто_відправлення, depart_date as Дата_відправлення, ticket_price as Ціна  from TicketInfo, City, FlightInfo Where FlightInfo.id_flight = TicketInfo.id_flight and FlightInfo.id_city_from = City.id_city and TicketInfo.id_client = {id_client}", connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    dataGridView1.DataSource = ds.Tables[0];
                    dataGridView1.Columns[0].Visible = false;
                }

                PaintRows();
            }

        }

        private void PaintRows()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToDateTime(row.Cells[3].Value) >= DateTime.Now.Date)
                    row.DefaultCellStyle.BackColor = Color.LawnGreen;
                else row.DefaultCellStyle.BackColor = Color.Pink;
            }
        }

        private void del_butt_Click(object sender, EventArgs e)
        {
            
                if (Convert.ToDateTime(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].Value) >= DateTime.Now.Date)
                {
                DialogResult answer = MessageBox.Show("Ви дійсно хочете повернути квиток?", "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer == DialogResult.Yes)
                {
                    ticketInfoBindingSource.Filter = $"id_flight = '{dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value}'";
                    ticketInfoBindingSource.RemoveCurrent();
                    ticketInfoBindingSource.EndEdit();
                    ticketInfoTableAdapter.Update(bus_TicketsDataSet);
                    dataGridView1.Update();
                    this.Update();
                }
                }
                else
                {
                DialogResult answer = MessageBox.Show("Ви дійсно хочете повернути квиток за 40% вартості?", "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer == DialogResult.Yes)
                {
                    ticketInfoBindingSource.Filter = $"id_flight = '{dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value}'";
                    ticketInfoBindingSource.RemoveCurrent();
                    ticketInfoBindingSource.EndEdit();
                    ticketInfoTableAdapter.Update(bus_TicketsDataSet);
                    dataGridView1.Update();
                    this.Update();               
                }
                }
        }

        private void back_butt_Click(object sender, EventArgs e)
        {
            MainForm mform = new MainForm();
            if (this.Text == "Адміністратор")
            {
                mform.Text = "Адміністратор";
                mform.edit_butt.Visible = true;
            }
            else mform.Text = "Користувач";
            this.Hide();
            mform.Show();
        }


        private void HomeForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TB_name.Enabled = false;
            TB_phone.Enabled = false;
            TB_surname.Enabled = false;
            save_butt.Visible = false;
            cancel_butt.Visible = false;
        }

        private void TB_phone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            del_butt.Enabled = true;
        }
    }
}
