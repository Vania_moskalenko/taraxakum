﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusTickets
{
    public partial class FlightEdit : Form
    {
        public FlightEdit()
        {
            InitializeComponent();
        }

        private void FlightEdit_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.City' table. You can move, or remove it, as needed.
            this.cityTableAdapter.Fill(this.bus_TicketsDataSet.City);
            this.ControlBox = false;
        }

        private void save_butt_Click(object sender, EventArgs e)
        {
            string connectString = ConfigurationManager.ConnectionStrings["BusTickets.Properties.Settings.Bus_TicketsConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectString);
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                con.Open();
                string sql = "SELECT * FROM FlightInfo";
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                System.Data.DataTable dt = ds.Tables[0];
                DataRow newRow = dt.NewRow();
                try
                {
                    newRow["flight_number"] = Convert.ToInt32(textBox1.Text);
                    newRow["id_city_from"] = Convert.ToInt32(comboBox1.SelectedValue);
                    newRow["id_city_to"] = Convert.ToInt32(comboBox2.SelectedValue);
                    newRow["seats_amount"] = Convert.ToInt32(numericUpDown1.Value);
                    newRow["depart_date"] = dateTimePicker1.Value.Date;
                    newRow["depart_time"] = Convert.ToDateTime(maskedTextBox1.Text).TimeOfDay;
                    newRow["arrived_time"] = Convert.ToDateTime(maskedTextBox2.Text).TimeOfDay;
                    newRow["distance"] = Convert.ToInt32(numericUpDown2.Value);
                    newRow["ticket_price"] = Convert.ToInt32(numericUpDown3.Value);
                    dt.Rows.Add(newRow);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
                    adapter.Update(dt);
                    ds.Clear();
                }
                catch (Exception)
                {
                    MessageBox.Show("Некорректні дані!!!");
                }
                MainForm mform = new MainForm();
                mform.Text = "Адміністратор";
                mform.edit_butt.Visible = true;
                this.Hide();
                mform.Show();
            }
        }

        private void back_butt_Click(object sender, EventArgs e)
        {
            MainForm mform = new MainForm();
            mform.Text = "Адміністратор";
            mform.edit_butt.Visible = true;
            this.Hide();
            mform.Show();
        }
    }
}
