﻿namespace BusTickets
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idflightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flightnumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idcityfromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bus_TicketsDataSet = new BusTickets.Bus_TicketsDataSet();
            this.idcitytoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cityBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.seatsamountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departtimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.arrivedtimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distanceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ticketpriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.flightInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.flightInfoTableAdapter = new BusTickets.Bus_TicketsDataSetTableAdapters.FlightInfoTableAdapter();
            this.cityTableAdapter = new BusTickets.Bus_TicketsDataSetTableAdapters.CityTableAdapter();
            this.CB_cityFrom = new System.Windows.Forms.ComboBox();
            this.CB_cityTo = new System.Windows.Forms.ComboBox();
            this.CB_flightNumber = new System.Windows.Forms.ComboBox();
            this.flightInfoBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.DTP_depart = new System.Windows.Forms.DateTimePicker();
            this.search_button = new System.Windows.Forms.Button();
            this.clear_button = new System.Windows.Forms.Button();
            this.home_butt = new System.Windows.Forms.Button();
            this.edit_butt = new System.Windows.Forms.Button();
            this.buy_butt = new System.Windows.Forms.Button();
            this.check_flight = new System.Windows.Forms.CheckBox();
            this.check_from = new System.Windows.Forms.CheckBox();
            this.check_to = new System.Windows.Forms.CheckBox();
            this.check_date = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bus_TicketsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flightInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flightInfoBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idflightDataGridViewTextBoxColumn,
            this.flightnumberDataGridViewTextBoxColumn,
            this.idcityfromDataGridViewTextBoxColumn,
            this.idcitytoDataGridViewTextBoxColumn,
            this.seatsamountDataGridViewTextBoxColumn,
            this.departdateDataGridViewTextBoxColumn,
            this.departtimeDataGridViewTextBoxColumn,
            this.arrivedtimeDataGridViewTextBoxColumn,
            this.distanceDataGridViewTextBoxColumn,
            this.ticketpriceDataGridViewTextBoxColumn});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.DataSource = this.flightInfoBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(14, 47);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(763, 285);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // idflightDataGridViewTextBoxColumn
            // 
            this.idflightDataGridViewTextBoxColumn.DataPropertyName = "id_flight";
            this.idflightDataGridViewTextBoxColumn.HeaderText = "id_flight";
            this.idflightDataGridViewTextBoxColumn.Name = "idflightDataGridViewTextBoxColumn";
            this.idflightDataGridViewTextBoxColumn.ReadOnly = true;
            this.idflightDataGridViewTextBoxColumn.Visible = false;
            // 
            // flightnumberDataGridViewTextBoxColumn
            // 
            this.flightnumberDataGridViewTextBoxColumn.DataPropertyName = "flight_number";
            this.flightnumberDataGridViewTextBoxColumn.HeaderText = "Рейс";
            this.flightnumberDataGridViewTextBoxColumn.Name = "flightnumberDataGridViewTextBoxColumn";
            this.flightnumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.flightnumberDataGridViewTextBoxColumn.Width = 70;
            // 
            // idcityfromDataGridViewTextBoxColumn
            // 
            this.idcityfromDataGridViewTextBoxColumn.DataPropertyName = "id_city_from";
            this.idcityfromDataGridViewTextBoxColumn.DataSource = this.cityBindingSource;
            this.idcityfromDataGridViewTextBoxColumn.DisplayMember = "city_name";
            this.idcityfromDataGridViewTextBoxColumn.HeaderText = "Звідки";
            this.idcityfromDataGridViewTextBoxColumn.Name = "idcityfromDataGridViewTextBoxColumn";
            this.idcityfromDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcityfromDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idcityfromDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idcityfromDataGridViewTextBoxColumn.ValueMember = "id_city";
            // 
            // cityBindingSource
            // 
            this.cityBindingSource.DataMember = "City";
            this.cityBindingSource.DataSource = this.bus_TicketsDataSet;
            // 
            // bus_TicketsDataSet
            // 
            this.bus_TicketsDataSet.DataSetName = "Bus_TicketsDataSet";
            this.bus_TicketsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // idcitytoDataGridViewTextBoxColumn
            // 
            this.idcitytoDataGridViewTextBoxColumn.DataPropertyName = "id_city_to";
            this.idcitytoDataGridViewTextBoxColumn.DataSource = this.cityBindingSource1;
            this.idcitytoDataGridViewTextBoxColumn.DisplayMember = "city_name";
            this.idcitytoDataGridViewTextBoxColumn.HeaderText = "Куди";
            this.idcitytoDataGridViewTextBoxColumn.Name = "idcitytoDataGridViewTextBoxColumn";
            this.idcitytoDataGridViewTextBoxColumn.ReadOnly = true;
            this.idcitytoDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idcitytoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idcitytoDataGridViewTextBoxColumn.ValueMember = "id_city";
            // 
            // cityBindingSource1
            // 
            this.cityBindingSource1.DataMember = "City";
            this.cityBindingSource1.DataSource = this.bus_TicketsDataSet;
            // 
            // seatsamountDataGridViewTextBoxColumn
            // 
            this.seatsamountDataGridViewTextBoxColumn.DataPropertyName = "seats_amount";
            this.seatsamountDataGridViewTextBoxColumn.HeaderText = "Місця";
            this.seatsamountDataGridViewTextBoxColumn.Name = "seatsamountDataGridViewTextBoxColumn";
            this.seatsamountDataGridViewTextBoxColumn.ReadOnly = true;
            this.seatsamountDataGridViewTextBoxColumn.Width = 50;
            // 
            // departdateDataGridViewTextBoxColumn
            // 
            this.departdateDataGridViewTextBoxColumn.DataPropertyName = "depart_date";
            this.departdateDataGridViewTextBoxColumn.HeaderText = "Дата відправлення";
            this.departdateDataGridViewTextBoxColumn.Name = "departdateDataGridViewTextBoxColumn";
            this.departdateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // departtimeDataGridViewTextBoxColumn
            // 
            this.departtimeDataGridViewTextBoxColumn.DataPropertyName = "depart_time";
            this.departtimeDataGridViewTextBoxColumn.HeaderText = "Час відправлення";
            this.departtimeDataGridViewTextBoxColumn.Name = "departtimeDataGridViewTextBoxColumn";
            this.departtimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.departtimeDataGridViewTextBoxColumn.Width = 90;
            // 
            // arrivedtimeDataGridViewTextBoxColumn
            // 
            this.arrivedtimeDataGridViewTextBoxColumn.DataPropertyName = "arrived_time";
            this.arrivedtimeDataGridViewTextBoxColumn.HeaderText = "Прибуття";
            this.arrivedtimeDataGridViewTextBoxColumn.Name = "arrivedtimeDataGridViewTextBoxColumn";
            this.arrivedtimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // distanceDataGridViewTextBoxColumn
            // 
            this.distanceDataGridViewTextBoxColumn.DataPropertyName = "distance";
            this.distanceDataGridViewTextBoxColumn.HeaderText = "Відстань";
            this.distanceDataGridViewTextBoxColumn.Name = "distanceDataGridViewTextBoxColumn";
            this.distanceDataGridViewTextBoxColumn.ReadOnly = true;
            this.distanceDataGridViewTextBoxColumn.Width = 60;
            // 
            // ticketpriceDataGridViewTextBoxColumn
            // 
            this.ticketpriceDataGridViewTextBoxColumn.DataPropertyName = "ticket_price";
            this.ticketpriceDataGridViewTextBoxColumn.HeaderText = "Ціна";
            this.ticketpriceDataGridViewTextBoxColumn.Name = "ticketpriceDataGridViewTextBoxColumn";
            this.ticketpriceDataGridViewTextBoxColumn.ReadOnly = true;
            this.ticketpriceDataGridViewTextBoxColumn.Width = 50;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(124, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItem1.Text = "Видалити";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // flightInfoBindingSource
            // 
            this.flightInfoBindingSource.DataMember = "FlightInfo";
            this.flightInfoBindingSource.DataSource = this.bus_TicketsDataSet;
            // 
            // flightInfoTableAdapter
            // 
            this.flightInfoTableAdapter.ClearBeforeFill = true;
            // 
            // cityTableAdapter
            // 
            this.cityTableAdapter.ClearBeforeFill = true;
            // 
            // CB_cityFrom
            // 
            this.CB_cityFrom.DataSource = this.cityBindingSource;
            this.CB_cityFrom.DisplayMember = "city_name";
            this.CB_cityFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_cityFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CB_cityFrom.FormattingEnabled = true;
            this.CB_cityFrom.Location = new System.Drawing.Point(227, 394);
            this.CB_cityFrom.Name = "CB_cityFrom";
            this.CB_cityFrom.Size = new System.Drawing.Size(144, 24);
            this.CB_cityFrom.TabIndex = 1;
            this.CB_cityFrom.ValueMember = "id_city";
            // 
            // CB_cityTo
            // 
            this.CB_cityTo.DataSource = this.cityBindingSource1;
            this.CB_cityTo.DisplayMember = "city_name";
            this.CB_cityTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_cityTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CB_cityTo.FormattingEnabled = true;
            this.CB_cityTo.Location = new System.Drawing.Point(484, 349);
            this.CB_cityTo.Name = "CB_cityTo";
            this.CB_cityTo.Size = new System.Drawing.Size(148, 24);
            this.CB_cityTo.TabIndex = 2;
            this.CB_cityTo.ValueMember = "id_city";
            // 
            // CB_flightNumber
            // 
            this.CB_flightNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_flightNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CB_flightNumber.FormattingEnabled = true;
            this.CB_flightNumber.Location = new System.Drawing.Point(227, 349);
            this.CB_flightNumber.Name = "CB_flightNumber";
            this.CB_flightNumber.Size = new System.Drawing.Size(144, 24);
            this.CB_flightNumber.TabIndex = 3;
            // 
            // flightInfoBindingSource1
            // 
            this.flightInfoBindingSource1.DataMember = "FlightInfo";
            this.flightInfoBindingSource1.DataSource = this.bus_TicketsDataSet;
            // 
            // DTP_depart
            // 
            this.DTP_depart.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.flightInfoBindingSource, "depart_date", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.DTP_depart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DTP_depart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTP_depart.Location = new System.Drawing.Point(484, 394);
            this.DTP_depart.Name = "DTP_depart";
            this.DTP_depart.Size = new System.Drawing.Size(148, 23);
            this.DTP_depart.TabIndex = 4;
            this.DTP_depart.Value = new System.DateTime(2019, 10, 15, 0, 0, 0, 0);
            // 
            // search_button
            // 
            this.search_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("search_button.BackgroundImage")));
            this.search_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.search_button.Location = new System.Drawing.Point(67, 342);
            this.search_button.Name = "search_button";
            this.search_button.Size = new System.Drawing.Size(71, 34);
            this.search_button.TabIndex = 5;
            this.search_button.UseVisualStyleBackColor = true;
            this.search_button.Click += new System.EventHandler(this.search_button_Click);
            // 
            // clear_button
            // 
            this.clear_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clear_button.BackgroundImage")));
            this.clear_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.clear_button.Location = new System.Drawing.Point(67, 384);
            this.clear_button.Name = "clear_button";
            this.clear_button.Size = new System.Drawing.Size(71, 34);
            this.clear_button.TabIndex = 6;
            this.clear_button.UseVisualStyleBackColor = true;
            this.clear_button.Click += new System.EventHandler(this.clear_button_Click);
            // 
            // home_butt
            // 
            this.home_butt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("home_butt.BackgroundImage")));
            this.home_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.home_butt.Location = new System.Drawing.Point(723, 3);
            this.home_butt.Name = "home_butt";
            this.home_butt.Size = new System.Drawing.Size(54, 38);
            this.home_butt.TabIndex = 7;
            this.home_butt.UseVisualStyleBackColor = true;
            this.home_butt.Click += new System.EventHandler(this.home_butt_Click);
            // 
            // edit_butt
            // 
            this.edit_butt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("edit_butt.BackgroundImage")));
            this.edit_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.edit_butt.Location = new System.Drawing.Point(662, 3);
            this.edit_butt.Name = "edit_butt";
            this.edit_butt.Size = new System.Drawing.Size(55, 38);
            this.edit_butt.TabIndex = 8;
            this.edit_butt.UseVisualStyleBackColor = true;
            this.edit_butt.Visible = false;
            this.edit_butt.Click += new System.EventHandler(this.edit_butt_Click);
            // 
            // buy_butt
            // 
            this.buy_butt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buy_butt.BackgroundImage")));
            this.buy_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buy_butt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buy_butt.Location = new System.Drawing.Point(681, 357);
            this.buy_butt.Name = "buy_butt";
            this.buy_butt.Size = new System.Drawing.Size(77, 53);
            this.buy_butt.TabIndex = 9;
            this.buy_butt.UseVisualStyleBackColor = true;
            this.buy_butt.Click += new System.EventHandler(this.buy_butt_Click);
            // 
            // check_flight
            // 
            this.check_flight.AutoSize = true;
            this.check_flight.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.check_flight.Checked = true;
            this.check_flight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_flight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.check_flight.Location = new System.Drawing.Point(168, 352);
            this.check_flight.Name = "check_flight";
            this.check_flight.Size = new System.Drawing.Size(55, 17);
            this.check_flight.TabIndex = 10;
            this.check_flight.Text = "Рейс";
            this.check_flight.UseVisualStyleBackColor = true;
            // 
            // check_from
            // 
            this.check_from.AutoSize = true;
            this.check_from.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.check_from.Checked = true;
            this.check_from.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_from.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.check_from.Location = new System.Drawing.Point(158, 398);
            this.check_from.Name = "check_from";
            this.check_from.Size = new System.Drawing.Size(65, 17);
            this.check_from.TabIndex = 11;
            this.check_from.Text = "Звідки";
            this.check_from.UseVisualStyleBackColor = true;
            // 
            // check_to
            // 
            this.check_to.AutoSize = true;
            this.check_to.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.check_to.Checked = true;
            this.check_to.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_to.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.check_to.Location = new System.Drawing.Point(424, 352);
            this.check_to.Name = "check_to";
            this.check_to.Size = new System.Drawing.Size(54, 17);
            this.check_to.TabIndex = 12;
            this.check_to.Text = "Куди";
            this.check_to.UseVisualStyleBackColor = true;
            // 
            // check_date
            // 
            this.check_date.AutoSize = true;
            this.check_date.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.check_date.Checked = true;
            this.check_date.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_date.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.check_date.Location = new System.Drawing.Point(422, 399);
            this.check_date.Name = "check_date";
            this.check_date.Size = new System.Drawing.Size(56, 17);
            this.check_date.TabIndex = 13;
            this.check_date.Text = "Дата";
            this.check_date.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(32, 331);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(625, 96);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 428);
            this.Controls.Add(this.check_date);
            this.Controls.Add(this.check_to);
            this.Controls.Add(this.check_from);
            this.Controls.Add(this.check_flight);
            this.Controls.Add(this.buy_butt);
            this.Controls.Add(this.edit_butt);
            this.Controls.Add(this.home_butt);
            this.Controls.Add(this.clear_button);
            this.Controls.Add(this.search_button);
            this.Controls.Add(this.DTP_depart);
            this.Controls.Add(this.CB_flightNumber);
            this.Controls.Add(this.CB_cityTo);
            this.Controls.Add(this.CB_cityFrom);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(805, 455);
            this.MinimumSize = new System.Drawing.Size(805, 455);
            this.Name = "MainForm";
            this.Text = "Рейси";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bus_TicketsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flightInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flightInfoBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridView1;
        public Bus_TicketsDataSet bus_TicketsDataSet;
        public System.Windows.Forms.BindingSource flightInfoBindingSource;
        public Bus_TicketsDataSetTableAdapters.FlightInfoTableAdapter flightInfoTableAdapter;
        public System.Windows.Forms.BindingSource cityBindingSource;
        public Bus_TicketsDataSetTableAdapters.CityTableAdapter cityTableAdapter;
        public System.Windows.Forms.BindingSource cityBindingSource1;
        private System.Windows.Forms.ComboBox CB_cityFrom;
        private System.Windows.Forms.ComboBox CB_cityTo;
        private System.Windows.Forms.ComboBox CB_flightNumber;
        public System.Windows.Forms.BindingSource flightInfoBindingSource1;
        private System.Windows.Forms.DateTimePicker DTP_depart;
        private System.Windows.Forms.Button search_button;
        private System.Windows.Forms.Button clear_button;
        private System.Windows.Forms.Button home_butt;
        public System.Windows.Forms.Button edit_butt;
        private System.Windows.Forms.Button buy_butt;
        private System.Windows.Forms.DataGridViewTextBoxColumn idflightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn flightnumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idcityfromDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn idcitytoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seatsamountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn departdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn departtimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn arrivedtimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn distanceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ticketpriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.CheckBox check_flight;
        private System.Windows.Forms.CheckBox check_from;
        private System.Windows.Forms.CheckBox check_to;
        private System.Windows.Forms.CheckBox check_date;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}