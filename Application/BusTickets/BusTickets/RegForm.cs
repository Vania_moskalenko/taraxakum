﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusTickets
{
    public partial class RegForm : Form
    {
        int id_user = 1;
        public RegForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AutorForm aform = new AutorForm();
            this.Hide();
            aform.Show();
        }

        public void reg_butt_Click(object sender, EventArgs e)
        {
            try
            {
                string connectString = ConfigurationManager.ConnectionStrings["BusTickets.Properties.Settings.Bus_TicketsConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectString))
            {
                
                string sql = "SELECT * FROM ClientInfo";
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                accountBindingSource.MoveLast();
                System.Data.DataTable dt = ds.Tables[0];
                DataRow newRow = dt.NewRow();
                 
                var tmpPhone = textBox5.Text.Replace(" ", "");
                var tmpName = textBox3.Text.Replace(" ", "");
                var tmpSurname = textBox4.Text.Replace(" ", "");
               
                
                string phonePattern = @"^[0][1-9]{2}\d{3}\d{2}\d{2}$";
                    string namePattern = @"[\d!@#$%^&*()_+-=:;',./<>?\\|\/]";


                    if (Regex.IsMatch(tmpPhone, phonePattern) && Regex.IsMatch(tmpName, namePattern)==false && Regex.IsMatch(tmpSurname, namePattern)==false)
                    {
                        newRow["id_user"] = id_user;
                        newRow["name"] = tmpName;
                        newRow["surname"] = tmpSurname;
                        newRow["age"] = Convert.ToInt32(textBox6.Text);
                        newRow["phone"] = tmpPhone;
                        dt.Rows.Add(newRow);
                        SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
                        adapter.Update(dt);
                        ds.Clear();
                        AutorForm aform = new AutorForm();
                        this.Hide();
                        aform.Show();
                        MessageBox.Show("Користувач успішно зареєстрований");
                    }
                    else MessageBox.Show("Некоректні особисті дані!!!");
                
            }
             }
                catch (Exception)
            {
                MessageBox.Show("Некорректні дані!!!");
            }


        }

        private void RegForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bus_TicketsDataSet.Account' table. You can move, or remove it, as needed.
            this.accountTableAdapter.Fill(this.bus_TicketsDataSet.Account);

        }

        public void next_butt_Click(object sender, EventArgs e)
        {
            
            string connectString = ConfigurationManager.ConnectionStrings["BusTickets.Properties.Settings.Bus_TicketsConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connectString);
            try
            {
                using (SqlConnection connection = new SqlConnection(connectString))
                {
                    con.Open();
                    string sql = "SELECT * FROM Account";
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);

                    System.Data.DataTable dt = ds.Tables[0];
                    DataRow newRow = dt.NewRow();


                    newRow["login"] = textBox1.Text;
                    newRow["password"] = textBox2.Text;
                    newRow["role"] = "user";

                    dt.Rows.Add(newRow);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
                    adapter.Update(dt);
                    SqlCommand command = new SqlCommand($"select id_user from Account where login = '{textBox1.Text}'", con);
                    id_user = Convert.ToInt32(command.ExecuteScalar());
                    ds.Clear();

                }
            }
            catch
            {
                MessageBox.Show("Некоректні дані!!!");
            }
                back_butt.Location = new Point(17, 179);
                reg_butt.Visible = true;
                next_butt.Visible = false;
                label1.Visible = false;
                label2.Visible = false;
                label3.Visible = true;
                label4.Visible = true;
                label5.Visible = true;
                label6.Visible = true;
                textBox1.Visible = false;
                textBox2.Visible = false;
                textBox3.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = true;
                textBox6.Visible = true;
                this.MaximumSize = new Size(275, 260);
                this.MinimumSize = new Size(275, 260);
                this.Size = new Size(275, 260);
            
        }
    }
}
